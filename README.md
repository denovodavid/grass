# Grass

Playing around with instanced rendering, shaders, and environment visuals.

## Resources

### Grass

- god-tier:
    - [Procedural Grass in 'Ghost of Tsushima'](https://youtu.be/Ibe1JBF5i5Y)
    - [Procedural grass rendering](https://outerra.blogspot.com/2012/05/procedural-grass-rendering.html)

- the grass diaries:
    1. [How Do Games Render So Much Grass?](https://youtu.be/Y0Ko0kvwfgA)
    2. [Modern Foliage Rendering](https://youtu.be/jw00MbIJcrk)
    3. [What I Did To Optimize My Game's Grass](https://youtu.be/PNvlqsXdQic)

- pretty cool:
    - [Godot 3/4 Particle Shader to Make 'Wandering' Grass With (Or Other Foliage)](https://youtu.be/YCBt-55PaOc)
    - [Forest Scene in Godot 4 (Alpha 6) - Terrain, Placement & Assets](https://youtu.be/1ho6tbxGt4c)

- maybe not the best:
    - [How to Render MILLIONS of Blades of Grass Efficiently in Godot](https://youtu.be/z_Q0sA3wE10)
    - [Simple Interactive Grass in Godot](https://youtu.be/qcScJ_vgsGU)
    - [UE4 Breakdown - How I create grass for my indie game Lucen](https://youtu.be/VJMd4d4viQ8)
    - [BotW STYLE GRASS TUTORIAL](https://youtu.be/usdwhhZWIJ4)

### Other

- [Wind Simulation in God of War](https://youtu.be/dDgyBKkSf7A)
- [Between Tech and Art: The Vegetation of Horizon Zero Dawn](https://youtu.be/wavnKZNSYqU)
- [GPU-Based Run-Time Procedural Placement in Horizon: Zero Dawn](https://youtu.be/ToCozpl1sYY)

## HEY! I'm currently rebuilding this in **Godot 4**, but the screenshot below are from my **Unity** tests.

![day grass](./day.jpg)

![night grass](./night.jpg)
